from itertools import cycle
from random import Random
from time import time
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from tictactoe.ruleset import TicTacToeClassic
    from tictactoe.player import Player


def simulate(iterations: int, p1: "Player", p2: "Player", game: "TicTacToeClassic", rnd = Random()):
    players = cycle([p1, p2])

    for i in range(iterations):
        while not game.is_finished():
            player = next(players)
            game.make_move(player, rnd.choice(game.cells_available))
        game.write()
        game.reset()


def simulate_with_progress(iterations: int, p1: "Player", p2: "Player", game: "TicTacToeClassic", start_time, rnd = Random()):
    simulate(iterations, p1, p2, game, rnd)
    print(f"{iterations} simulations in {time() - start_time} seg")

    print(f"\n{p1.name}")
    p1.print_stats()

    print(f"\n{p2.name}")
    p2.print_stats()

    p1.clear_progress()
    p2.clear_progress()