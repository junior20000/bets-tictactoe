# Bets TicTacToe
> Simulation homework

Just a TicTacToe game + a hypergrahp library made in about 1 hour.

## The Homework Requirements
- Run the simulation for 500, 1000, 2000 and 5000 matches
- Analyse the win / loss rate
- Make a good play evaluation process

## Deployment
No deployment instructions available for this project ATM.

## Contributing
The project is **NOT** open to contributions.

## Versioning
I use [GIT](https://git-scm.com/) for versioning.

## Author
 * [Roberto Schiavelli Júnior](https://www.linkedin.com/in/roberto-schiavelli-j%C3%BAnior-86a3561a9/) - November 2021

## Copyright
Roberto Schiavelli Júnior - All Rights Reserved  
Unauthorized copying of this project and any files within it, via any medium is strictly prohibited  
Proprietary and confidential