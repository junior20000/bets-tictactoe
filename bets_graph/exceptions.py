class GraphException(Exception):
    pass


class EdgeAlreadyExistsException(GraphException):

    def __init__(self, edge_name: str):
        self.edge_name = edge_name

    def __str__(self):
        return f"Edge <{self.edge_name}> already exists"


class VertexAlreadyExistsException(GraphException):

    def __init__(self, vertex_name: str):
        self.vertex_name = vertex_name

    def __str__(self):
        return f"Vertex <{self.vertex_name}> already exists"


class EdgeDoesNotExistException(GraphException):

    def __init__(self, edge_name: str):
        self.edge_name = edge_name

    def __str__(self):
        return f"Edge <{self.edge_name}> does not exist"


class VertexStillInEdgeException(GraphException):

    def __init__(self, vertex_name: str, edge_name: str):
        self.vertex_name = vertex_name
        self.edge_name = edge_name

    def __str__(self):
        return f"Vertex <{self.vertex_name}> still in edge <{self.edge_name}>"


class VertexNotInEdgeException(GraphException):

    def __init__(self, vertex_name: str, edge_name: str):
        self.vertex_name = vertex_name
        self.edge_name = edge_name

    def __str__(self):
        return f"Vertex <{self.vertex_name}> not in edge <{self.edge_name}>"
