from copy import deepcopy
from typing import Collection, Dict, List, Set
from weakref import ReferenceType, WeakSet, ref

from .exceptions import *


class HyperVertex:
    __slots__ = ("__weakref__", "_edges", "name")

    def __init__(self, name: str):
        self.name: str = name
        self._edges: WeakSet = WeakSet()

    def __eq__(self, other: "HyperVertex"):
        return True if self.name == other.name else False

    @property
    def edges(self):
        return self._edges

    def add_edge_to_cache(self, edge: "HyperEdge"):
        if self not in edge.vertices:
            raise VertexNotInEdgeException(self.name, edge.name)

        self._edges.add(edge)

    def remove_edge_from_cache(self, edge: "HyperEdge"):
        if self in edge.vertices:
            raise VertexStillInEdgeException(self.name, edge.name)

        self._edges.remove(edge)


class HyperEdge:
    __slots__ = ("__weakref__", "_vertices", "name")

    def __init__(self, name, vertices: Set[HyperVertex] = None):
        self.name = name
        self._vertices: WeakSet = WeakSet(*vertices) if vertices else WeakSet()

    def __len__(self):
        return len(self._vertices)

    @property
    def vertices(self):
        return self._vertices

    def add(self, vertex: HyperVertex) -> None:
        self._vertices.add(vertex)
        vertex.add_edge_to_cache(self)

    def intersection(self, *args: Collection["HyperEdge"]) -> Set[HyperVertex]:
        vertices = map(HyperEdge.vertices.fget, args)
        return self._vertices.intersection(vertices)

    def try_add(self, vertex: HyperVertex) -> None:
        if vertex in self._vertices:
            raise VertexAlreadyExistsException(vertex.name)

        self._vertices.add(vertex)
        vertex.add_edge_to_cache(self)

    def clear(self) -> None:
        self._vertices.clear()


class HyperGraph:
    __slots__ = ("_edges", "_vertices")

    def __init__(self, vertices: Collection[HyperVertex] = None, edges: Collection[HyperEdge] = None):
        self._vertices: Dict[str, HyperVertex] = {}
        self._edges: Dict[str, HyperEdge] = {}

        self.add_vertices(vertices)
        self.add_edges(edges)

    @property
    def vertices(self):  # TODO: RETURN WEAKDICT
        return self._vertices

    @property
    def edges(self):  # TODO: RETURN WEAKDICT
        return self._edges

    def add_edge(self, edge: HyperEdge):
        if edge.name not in self._edges.keys():
            self._edges[edge.name] = deepcopy(edge)

    def add_vertex(self, vertex: HyperVertex):
        if vertex.name not in self._vertices.keys():
            self._vertices[vertex.name] = deepcopy(vertex)

    def add_edges(self, edges: Collection[HyperEdge]):
        if not edges:
            return

        for edge in edges:
            self.add_edge(edge)

    def add_vertices(self, vertices: Collection[HyperVertex]):
        if not vertices:
            return

        for vertex in vertices:
            self.add_vertex(vertex)

    def add_vertex_to_edge(self, vertex_name: str, edge_name: str):  # TODO: Add vertex check
        if edge_name not in self._edges.keys():
            raise EdgeDoesNotExistException(edge_name)

        edge = self._edges[edge_name]
        edge.add(self._vertices[vertex_name])

    def add_vertices_to_edges(self, vertices_names: Collection[str], edges_names: Collection[str]):
        for vertex in vertices_names:
            for edge in edges_names:
                self.add_vertex_to_edge(vertex, edge)

    def get_vertex(self, vertex_name: str) -> ReferenceType:  # TODO: ADD "TRY GET VERTEX"
        return ref(self._vertices[vertex_name])

    def get_edge(self, vertex_name: str) -> ReferenceType:  # TODO: ADD "TRY GET EDGE"
        return ref(self._edges[vertex_name])

    def get_edges(self, *args: str):
        return [self._edges[arg] for arg in args]

    def try_add_edge(self, edge: HyperEdge):
        if edge.name in self._edges.keys():
            raise EdgeAlreadyExistsException(edge.name)

        self._edges[edge.name] = edge

    def try_add_vertex(self, vertex: HyperVertex):
        if vertex.name in self._vertices.keys():
            raise VertexAlreadyExistsException(vertex.name)

        self._vertices[vertex.name] = deepcopy(vertex)
