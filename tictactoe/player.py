from typing import Union
from weakref import proxy, ProxyType, ref, ReferenceType

from bets_graph.hypergraph import *

from .core import MatchResult
from .utils import incremental_avg
from .ttt_stats import MatchInfo


class Player:
    __slots__ = ("__weakref__", "_stats", "name", "symbol")

    def __init__(self, name: str, symbol: str):
        self.name = name
        self.symbol = symbol
        self._stats: "PlayerStats" = PlayerStats(self)

    def __eq__(self, other):
        if not isinstance(other, Player):
            return False

        return (self.name == other.name) and (self.symbol == other.symbol)

    def __hash__(self):
        return hash(self.name) + hash(self.symbol)

    @property
    def stats(self):
        return self._stats

    def append_match(self, match: MatchInfo):
        if match.result == MatchResult.VICTORY:
            if self in match.winners:
                self._stats.increment_victories()
            else:
                self._stats.increment_defeats()
        elif match.result == MatchResult.DRAW:
            self._stats.increment_draws()
        else:
            self._stats.increment_quits()

        self.stats.increment_avg_moves(match.moves[self.name])

    def clear_progress(self):
        self._stats.clear_stats()

    def print_stats(self):
        s = self._stats
        sstr = f"Victories: {s.victories}\n" \
               f"Defeats: {s.defeats}\n" \
               f"Draws: {s.draws}\n" \
               f"Quits: {s.quits}\n" \
               f"Win %: {s.win_rate * 100}%\n" \
               f"Lose %: {s.lose_rate * 100}%\n"\
               f"Draw %: {s.draw_rate * 100}%\n" \
               f"Avg. moves: {s.avg_moves}\n" \
               f"Total matches: {s.total_matches}\n"
        print(sstr)


class PlayerStats:
    __slots__ = ("_match_hist", "player", "__victories", "__defeats", "__quits", "__draws", "__total_matches", "__avg_moves")

    def __init__(self, player: "Player"):
        self.player = proxy(player)
        self.__victories: int = 0
        self.__defeats: int = 0
        self.__draws: int = 0
        self.__quits: int = 0
        self.__total_matches: int = 0
        self.__avg_moves: int = 0
        self._match_hist: List[MatchInfo] = []

    @property
    def victories(self):
        return self.__victories

    @victories.setter
    def victories(self, value: int):
        self.__victories = value
        self.__update_total_matches()

    @property
    def defeats(self):
        return self.__defeats

    @defeats.setter
    def defeats(self, value: int):
        self.__defeats = value
        self.__update_total_matches()

    @property
    def draws(self):
        return self.__draws

    @draws.setter
    def draws(self, value: int):
        self.__draws = value
        self.__update_total_matches()

    @property
    def quits(self):
        return self.__quits

    @quits.setter
    def quits(self, value):
        self.__quits = value
        self.__update_total_matches()

    @property
    def avg_moves(self):
        return self.__avg_moves

    @property
    def total_matches(self):
        return self.__total_matches

    @property
    def win_rate(self):
        return self.__victories / self.__total_matches

    @property
    def lose_rate(self):
        return self.__defeats / self.__total_matches

    @property
    def draw_rate(self):
        return self.__draws / self.__total_matches

    @property
    def match_hist(self):
        return self._match_hist

    def add_match(self, match: "MatchInfo"):
        self._match_hist.append(match)

    def increment_victories(self):
        self.__victories += 1
        self.__total_matches += 1

    def increment_defeats(self):
        self.__defeats += 1
        self.__total_matches += 1

    def increment_draws(self):
        self.__draws += 1
        self.__total_matches += 1

    def increment_quits(self):
        self.__quits += 1
        self.__total_matches += 1

    def __update_total_matches(self):
        self.__total_matches = self.__victories + self.__defeats + self.__draws + self.quits

    def clear_stats(self):
        self.__victories: int = 0
        self.__defeats: int = 0
        self.__draws: int = 0
        self.__quits: int = 0
        self.__total_matches: int = 0

    def increment_avg_moves(self, num_moves):
        self.__avg_moves = incremental_avg(self.__avg_moves, num_moves, self.__total_matches)


class PlayerToken(HyperVertex):
    __slots__ = "_player"

    def __init__(self, name: str):
        super().__init__(name)
        self._player: Union[ProxyType, None] = None

    def __hash__(self):
        return hash(self.name) + hash(self.player)

    @property
    def player(self):
        return self._player

    @player.setter
    def player(self, player: "Player"):
        self._player = proxy(player)

    def is_filled(self) -> bool:
        return True if self._player else False

    def clear(self):
        self._player = None
