from functools import lru_cache
from typing import Any, Iterable


@lru_cache(typed=True)
def all_equal(container: Iterable) -> bool:
    iterator = iter(container)

    try:
        first = next(iterator)
    except StopIteration:
        return True

    return all(first == x for x in iterator)


@lru_cache(typed=True)
def all_equal_to(container: Iterable, to: Any) -> bool:
    iterator = iter(container)

    return all(to == x for x in iterator)


def incremental_avg(avg, x, n):
    return avg + (x - avg) / n
