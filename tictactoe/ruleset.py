import itertools
from typing import List, Dict, Tuple, Union
from weakref import ref, ProxyType

from bets_graph.hypergraph import HyperEdge, HyperGraph, HyperVertex

from .core import MatchResult, MatchState, WinningLine
from .exceptions import *
from .player import Player, PlayerToken
from .ttt_stats import MatchInfo


# TODO: Separate "ruleset" from "board"
class TicTacToeClassic:
    __slots__ = ("_board_meta", "_play_cache", "__move_hist", "p1", "p2", "_state", "__match_info")

    # TODO: Change p1 and p2, player container may be well better
    def __init__(self, p1: Player, p2: Player):
        self._board_meta: HyperGraph = HyperGraph()
        self._play_cache = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.__move_hist: List[Tuple["Player", int]] = []
        self.p1 = p1
        self.p2 = p2
        self._state = MatchState.IDLE
        self.__match_info: Union["MatchInfo", None] = None

        self.__init_board_meta()

    @property
    def cells_available(self):
        return self._play_cache

    @property
    def winner(self):
        if not self.__match_info or (not self.__match_info.winners):
            return None
        return self.__match_info.winners[0]

    @property
    def state(self):
        return self._state

    @property
    def result(self):
        return MatchResult.UNDEFINED if not self.__match_info else self.__match_info.result

    def __init_board_meta(self):
        ptks = [PlayerToken(f"cell{i + 1}") for i in range(9)]

        wrs = [WinningLine("r1"), WinningLine("r2"), WinningLine("r3"),
               WinningLine("c1"), WinningLine("c2"), WinningLine("c3"),
               WinningLine("md"), WinningLine("sd")]

        self._board_meta.add_vertices(ptks)
        self._board_meta.add_edges(wrs)

        self._board_meta.add_vertices_to_edges(["cell1"], ["r1", "c1", "md"])
        self._board_meta.add_vertices_to_edges(["cell2"], ["r1", "c2"])
        self._board_meta.add_vertices_to_edges(["cell3"], ["r1", "c3", "sd"])
        self._board_meta.add_vertices_to_edges(["cell4"], ["r2", "c1"])
        self._board_meta.add_vertices_to_edges(["cell5"], ["r2", "c2", "md", "sd"])
        self._board_meta.add_vertices_to_edges(["cell6"], ["r2", "c3"])
        self._board_meta.add_vertices_to_edges(["cell7"], ["r3", "c1", "sd"])
        self._board_meta.add_vertices_to_edges(["cell8"], ["r3", "c2"])
        self._board_meta.add_vertices_to_edges(["cell9"], ["r3", "c3", "md"])

    def finish(self):
        if not self.__match_info:
            self.__match_info = MatchInfo("classic", [self.p1, self.p2], None, MatchResult.CANCELED)

        self._state = MatchState.FINISHED

    def is_finished(self):
        return self._state == MatchState.FINISHED

    def is_board_empty(self):
        pts: Dict[str, "PlayerToken"] = self._board_meta.vertices

        return all((pt.player is None for pt in pts.values()))

    def get_winning_line(self, player_token: PlayerToken) -> Union[WinningLine, None]:
        for wl in player_token.edges:
            if wl.check_win():
                return wl

    def make_move(self, player: Player, cell: int):
        if self.is_finished():
            raise GameAlreadyFinishedException
        if (cell > len(self._board_meta.vertices)) or (cell <= 0):
            raise ValueError(f"Cell value must be from 1 to 9. Received '{cell}'")

        pt = self._board_meta.get_vertex(f"cell{cell}")

        if pt().player:
            raise CellAlreadyFilledException(cell)

        pt().player = player
        wl = self.get_winning_line(pt())
        self.__move_hist.append((player, cell))

        if wl:
            self.__match_info = MatchInfo("classic", [self.p1, self.p2], [pt().player], MatchResult.VICTORY)
            self.finish()
            return

        self._play_cache.remove(cell)
        if not self._play_cache:
            self.__match_info = MatchInfo("classic", [self.p1, self.p2], result=MatchResult.DRAW)
            self.finish()
            return

    def print_board(self):
        cell = 0
        rcanvas = []

        for r in range(3):
            for i in range(3):
                cell += 1
                pt = self._board_meta.get_vertex(f"cell{cell}")
                rcanvas.append(pt().player.symbol if pt().player else " ")
            print(" | ".join(rcanvas))
            rcanvas.clear()

    def print_move_hist(self):
        for move in self.__move_hist:
            print(f"{move[0].name}: {move[1]}")

    def reset(self):
        self._play_cache = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.__move_hist = []
        self._state = MatchState.IDLE
        self.__match_info = None

        for pt in self._board_meta.vertices.values():
            pt.clear()

    def write(self):  # TODO: Rename this method
        mi = self.__match_info

        if not mi:
            return

        for p in [self.p1, self.p2]:
            moves = 0
            for move in self.__move_hist:
                if move[0].name == p.name:
                    moves += 1
            mi.moves[p.name] = moves

        self.p1.append_match(mi)
        self.p2.append_match(mi)
