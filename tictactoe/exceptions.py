class TicTacToeException(Exception):
    pass


class GameAlreadyFinishedException(TicTacToeException):
    def __str__(self):
        return f"This game is already finished, please restart"


class CellAlreadyFilledException(TicTacToeException):
    def __init__(self, cell: int):
        self.cell = cell

    def __str__(self):
        return f"Cell <{self.cell}> is already filled"
