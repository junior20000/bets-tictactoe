import itertools
from collections import defaultdict
from typing import TYPE_CHECKING, List, Union

from .core import MatchResult

if TYPE_CHECKING:
    from .player import Player


class MatchInfo:
    __id_factory = itertools.count()

    def __init__(self, mode: str, players: List["Player"], winners: Union[List["Player"], None] = None, result: "MatchResult" = MatchResult.UNDEFINED):
        self.id: int = next(self.__id_factory)
        self.__mode = mode
        self.__players = players
        self.__moves = defaultdict(lambda: 0)
        self.__result: str = result
        self.__winners: Union[List["Player"], None] = winners

    @property
    def mode(self):
        return self.__mode

    @property
    def players(self):
        return self.__players

    @property
    def moves(self):
        return self.__moves

    @property
    def result(self):
        return self.__result

    @property
    def winners(self):
        return self.__winners
