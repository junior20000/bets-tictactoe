from enum import Enum

from bets_graph.hypergraph import HyperEdge

from .utils import all_equal, all_equal_to


class MatchResult(Enum):
    UNDEFINED = "undefined",
    VICTORY = "victory",
    DRAW = "draw",
    CANCELED = "canceled"


class MatchState(Enum):
    IDLE = 0,
    IN_PROGRESS = 1,
    FINISHED = 2


class WinningLine(HyperEdge):

    def __init__(self, name: str):
        super().__init__(name)

    def is_empty(self) -> bool:
        gen = (vertex.player for vertex in self._vertices)
        return all(None is p for p in gen)

    def check_win(self) -> bool:
        return all_equal((vertex.player for vertex in self._vertices))

    def remove_players(self):
        for pt in self.vertices:
            pt().player = None
